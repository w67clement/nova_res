#include <glbinding/Binding.h>
#include <openw67render/input/ControllerManager.h>
#include <openw67render/graphics/OpenGraphics.h>
#include <openw67render/OpenContext.h>
#include <iostream>
#include "../dependencies/openw67render/dependencies/SOIL/inc/SOIL/SOIL.h"

using namespace std;
using namespace w67r;

void error_callback(int error, const char *description)
{
    fprintf(stderr, string("Error: %s\n" + to_string(error)).c_str(), description);
}

class ControllerListener : public ControllerBaseListener
{
public:
    void connect(Controller &controller)
    {
        cout << "Controller with ID '" << controller.getId() << "', and name '" << controller.getName()
             << "' is connected !" << endl;
    }

    void disconnect(Controller &controller)
    {
        cout << "Controller with ID '" << controller.getId() << "', and name '" << controller.getName()
             << "' is disconnected !" << endl;
    }
};

class Nu20Lo75Runnable : public Runnable
{
public:
    GLFWerrorfun glfwErrorCallback()
    {
        return error_callback;
    }

    void initError(string error)
    {
    }

    void init()
    {

    }

    void glInit()
    {

    }
};

int main(int argc, char **argv)
{
    bool run = true;
    cout << "Creating runnable..." << endl;
    Nu20Lo75Runnable runnable = Nu20Lo75Runnable();
    cout << "Initializing context" << endl;
    Context context = Context(&runnable);
    context.init();
    cout << "Adding CONTROLLER_MANAGER..." << endl;
    ControllerListener listener = ControllerListener();
    Controllers::CONTROLLER_MANAGER.addBaseListener(&listener);
    cout << "Creating window..." << endl;
    OpenWindow window = OpenWindow("nova_res", 880, 640, OpenMonitor::PRIMARY_MONITOR);

    if (!window.getWindowPointer())
        cerr << "Error when initialize the window." << endl;

    /* Make the window's context current */
    cout << "Making current OpenGL context to window..." << endl;
    glfwMakeContextCurrent(window.getWindowPointer());
    context.glInit();

    cout << "Creating graphics..." << endl;
    OpenGraphics graphics = OpenGraphics();
    int x(20), y(20), width(100), height(100);

    cout << "Loading fucking texture..." << endl;
    OpenTexture texture = loadTexture("portal.png");
    int textureId = texture.id;
    //int textureId = SOIL_load_OGL_texture("degrade.png", 4, SOIL_CREATE_NEW_ID, 0);

    cout << "TEXTURE: " << to_string(textureId) << endl;

    /* Loop until the user closes the window */
    cout << "Oh crap, we render..." << endl;
    while (run)
    {
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //glClearColor(1, 1, 1, 1);

        // ENABLE 2D RENDER
        glMatrixMode(GL_PROJECTION);
        glPushMatrix(); // preserve perspective view
        glLoadIdentity(); // clear the perspective matrix
        glOrtho( // turn on 2D mode
                //// viewportX,viewportX+viewportW, // left, right
                //// viewportY,viewportY+viewportH, // bottom, top !!!
                0, window.getWidth(), // left, right
                window.getHeight(), 0, // bottom, top
                -500, 500); // Zfar, Znear

        setViewport(0, 0, window.getWidth(), window.getHeight());

        graphics.setTransparency(true);

        // clear the modelview matrix
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix(); // Preserve the Modelview Matrix
        glLoadIdentity(); // clear the Modelview Matrix
        // disable depth test so further drawing will go over the current scene
        glDisable(GL_DEPTH_TEST);

        glEnable(GL_TEXTURE_2D);

        Color color = Color(71, 97, 141);
        graphics.setColor(color);
        graphics.beginDraw(DRAW_TYPE_QUADS);
        glTexCoord2f(0, 0);
        graphics.vertex2f(x, y);
        glTexCoord2f(0, 1);
        graphics.vertex2f(x, y + height);
        glTexCoord2f(1, 1);
        graphics.vertex2f(x + width, y + height);
        glTexCoord2f(1, 0);
        graphics.vertex2f(x + width, y);
        graphics.endDraw();

        graphics.drawLine2D(250, 250, 350, 350);

        graphics.setColor(Color::WHITE);
        graphics.drawImage(&texture, 350, 60, texture.width, texture.height);

        //graphics.setTexture2DOn(false);
        //graphics.setTransparency(false);
        //graphics.setOrthoOff();

        // Manual loading works

        /*glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, (GLuint) textureId);
        glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0);
        glVertex2f(250, 60 + 256);
        glTexCoord2f(0.0, 1.0);
        glVertex2f(250, 60);
        glTexCoord2f(1.0, 1.0);
        glVertex2f(250 + 256, 60);
        glTexCoord2f(1.0, 0.0);
        glVertex2f(250 + 256, 60 + 256);
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0);*/

        /* Swap front and back buffers */
        glfwSwapBuffers(window.getWindowPointer());

        /* Poll for and process events */
        glfwPollEvents();

        if (glfwWindowShouldClose(window.getWindowPointer()))
            run = false;
    }
    cout << "RT..." << endl;
    deleteTexture(&texture);
    glfwTerminate();

    return 0;
}